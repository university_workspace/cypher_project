/**
 * Manages the Web application (controller and view).
 * Manage the REST call about flights
 *
 * @author SpatialTeam
 * @version 1.00
 * @since 1.00
 */
package servlet;